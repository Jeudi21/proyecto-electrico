# Programa para la extracción de características y aplicación de métricas

## Ejecución del programa

El programa descrito se ejecuta en este directorio de la siguiente manera

```
python programa.py
```
## Directorios del programa

El archivo programa.py implementa una interfaz gráfica con las ventanas en el directorio "interfaz_grafica". La interfaz hace uso de las distintas funciones incluidas en el directorio "funciones" para la extracción de características y aplicación de métricas de tono y espectro a un conjunto de audios.
