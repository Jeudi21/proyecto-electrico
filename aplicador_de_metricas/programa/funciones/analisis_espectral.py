#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''Archivo que maneja toda la lógica para el análisis espectral.'''

import threading
import os
import Queue
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import funciones

def generar_tabla(archivo, cantidad_mfcc, lista_coeficientes):
    '''Genera una tabla con resultados ordenados por
    su valor de MFCC.'''

    # Si la cantidad de MFCC es impar, se agrega un valor
    # para mantener la misma distribución en ambas columnas.
    if cantidad_mfcc % 2:
        mfcc_por_columna = (cantidad_mfcc + 1) / 2
    else:
        mfcc_por_columna = cantidad_mfcc / 2

    # Se escriben los valores en la tabla.
    for numero_coeficiente in range(mfcc_por_columna):

        # El último valor de una cantidad impar de MFCC es un -.
        if cantidad_mfcc % 2 and numero_coeficiente == mfcc_por_columna - 1:
            archivo.write('MFCC ' + str(numero_coeficiente + 1) +
                          '&' + str(round(lista_coeficientes[numero_coeficiente], 3)) +
                          '&' 'MFCC ' + str(numero_coeficiente + 1 + mfcc_por_columna) +
                          '&' + '-')
            archivo.write('\\\\ \\hline\n')
        else:
            archivo.write('MFCC ' + str(numero_coeficiente + 1) +
                          '&' + str(round(lista_coeficientes[numero_coeficiente], 3)) +
                          '&' 'MFCC ' + str(numero_coeficiente + 1 + mfcc_por_columna) +
                          '&' + str(round(lista_coeficientes[numero_coeficiente + mfcc_por_columna], 3)))
            archivo.write('\\\\ \\hline\n')
    archivo.write('\\end{tabular} \n')
    archivo.write('\\end{table} \n')

def generar_informe_resumen(cantidad_audios, opciones,
                            ponderado_distancia_euclidea, ponderado_mad,
                            arreglo_ponderado_mad, ponderado_manhattan, nombre_salida,
                            cantidad_mfcc):
    '''Genera un informe resumen con los datos promedios de las métricas aplicadas
    a todos los archivos.'''

    # Indica si la imagen es para audios sucios o fitrados.
    if nombre_salida == funciones.INFORME_RESUMEN_ESPECTRAL_RUIDO:
        identificador_imagen = "ruido"
    else:
        identificador_imagen = "filtrado"

    informe_resumen = open(funciones.DIRECTORIOS["latex"] + nombre_salida, 'w')
    informe_resumen.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\'+
                          'usepackage{graphicx}\n\\usepackage{float}\n\\title{Informe resumen: '+
                          'Sistema de análisis espectral entre conjuntos de señales de habla: '+
                          'Métrica Distancia Euclídea, Distancia absoluta media (MAD) y Distancia '+
                          'de Manhattan}\n\\author{EIE}\n\\begin{document}\n\\maketitle\n')

    # Resumen distancia euclídea.
    if opciones[0]:

        # Inicio de la sección distancia euclídea.
        informe_resumen.write('\\newpage \n')
        informe_resumen.write('\\section{Distancia Euclídea} \n')

        #Inicio de tabla para latex
        informe_resumen.write('\\begin{table}[H] \n')
        informe_resumen.write('\\centering \n')
        informe_resumen.write('\\caption{Distancia euclídea}')
        informe_resumen.write('\\begin{tabular}{|l|l|l|l|}\n')
        informe_resumen.write('\\hline \n')
        informe_resumen.write('MFCC & Distancia euclídea & MFCC & Distancia euclídea \\\\')
        informe_resumen.write('\\hline \n')

        # Promedio de distancias euclídeas para todos los archivos analizados.
        ponderado_distancia_euclidea = ponderado_distancia_euclidea/cantidad_audios
        ponderado_distancia_euclidea = np.array(ponderado_distancia_euclidea).tolist()

        # Generación de tabla.
        generar_tabla(informe_resumen, cantidad_mfcc, ponderado_distancia_euclidea)

        # Generación de gráfico.
        cepstros = np.arange(len(ponderado_distancia_euclidea))
        plt.bar(cepstros, ponderado_distancia_euclidea)
        plt.title('Distancia Euclideana')
        plt.xlabel('Coeficientes Cepstrales')
        plt.ylabel('Distancia Euclideana')
        plt.savefig(funciones.DIRECTORIOS["pictures"] + '/DE'+ '_' + identificador_imagen + '.png')
        informe_resumen.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1'+
                              '\\textwidth]{' + funciones.DIRECTORIOS["pictures"] +\
                              '/DE'+ '_' + identificador_imagen +'.png' + '}\n')
        informe_resumen.write('\\caption{Gráfico de barras para distancia euclídea}\n \\'+
                              'label{fig:barraDM}\n \\end{figure}')
        plt.close()

    # Resumen MAD.
    if opciones[1]:

        # Promedio de MAD para todos los archivos analizados.
        arreglo_ponderado_mad = arreglo_ponderado_mad / cantidad_audios
        ponderado_mad = ponderado_mad / cantidad_audios
        arreglo_ponderado_mad = np.array(arreglo_ponderado_mad).tolist()

        # Inicio de la sección MAD.
        informe_resumen.write('\\newpage \n')
        informe_resumen.write('\\section{MAD}' + '\n\n')
        informe_resumen.write('\\textbf{Los resultados para la métrica MAD son: } ' + '\n\n')
        informe_resumen.write('\nMAD: ' + str(round(ponderado_mad, 3)) + '\n\n')
        informe_resumen.write('\\textbf{La distancia media absoluta es:} ' + '\n\n ')

        #Inicio de tabla.
        informe_resumen.write('\\begin{table}[H] \n')
        informe_resumen.write('\\centering \n')
        informe_resumen.write('\\caption{Distancia media absoluta}')
        informe_resumen.write('\\begin{tabular}{|l|l|l|l|}\n')
        informe_resumen.write('\\hline \n')
        informe_resumen.write('MFCC & MAD & MFCC & MAD \\\\')
        informe_resumen.write('\\hline \n')

        # Generación de tabla.
        generar_tabla(informe_resumen, cantidad_mfcc, arreglo_ponderado_mad)

        #Generación de gráfico.
        cepstros = np.arange(len(arreglo_ponderado_mad))
        plt.bar(cepstros, arreglo_ponderado_mad)
        plt.title('Distancia absoluta media')
        plt.xlabel('Coeficientes Cepstrales')
        plt.ylabel('MAD')
        plt.savefig(funciones.DIRECTORIOS["pictures"] + '/DAM'+ '_' + identificador_imagen +'.png')
        informe_resumen.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1'+
                              '\\textwidth]{' + funciones.DIRECTORIOS["pictures"] + '/DAM'+
                              '_' + identificador_imagen +'.png' + '}\n')
        informe_resumen.write('\\caption{Gráfico de barras para Distancia absoluta media}\n \\label'+
                              '{fig:barraDaM}\n \\end{figure}')
        plt.close()

    # Resumen distancia Manhattan.
    if opciones[2]:

        # Promedio de distancias Manhattan para todos los archivos analizados.
        ponderado_manhattan = ponderado_manhattan/cantidad_audios
        ponderado_manhattan = np.array(ponderado_manhattan).tolist()

        # Inicio de la sección distancia Manhattan.
        informe_resumen.write('\\newpage \n')
        informe_resumen.write('\\section{Distancia de Manhattan}' + '\n\n')

        #Inicio de tabla para latex
        informe_resumen.write('\\begin{table}[H] \n')
        informe_resumen.write('\\centering \n')
        informe_resumen.write('\\caption{Distancia de Manhattan}')
        informe_resumen.write('\\begin{tabular}{|l|l|l|l|}\n')
        informe_resumen.write('\\hline \n')
        informe_resumen.write('MFCC & Manhattan & MFCC & Manhattan \\\\')
        informe_resumen.write('\\hline \n')

        # Se genera la tabla
        generar_tabla(informe_resumen, cantidad_mfcc, ponderado_manhattan)

        # Se genera el gráfico.
        cepstros = np.arange(len(ponderado_manhattan))
        plt.bar(cepstros, ponderado_manhattan)
        plt.title('Distancia de Manhattan')
        plt.xlabel('Coeficientes Cepstrales')
        plt.ylabel('Manhattan')
        plt.savefig(funciones.DIRECTORIOS["pictures"] + '/DM'+ '_' + identificador_imagen +'.png')
        informe_resumen.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\'+
                              'textwidth]{' + funciones.DIRECTORIOS["pictures"] + '/DM'+
                              '_' + identificador_imagen +'.png' + '}\n')
        informe_resumen.write('\\caption{Gráfico de barras para Distancia de Manhattan}\n \\label'+
                              '{fig:barraDM}\n \\end{figure}')
        plt.close()

    # Se escribe la cantidad final de archivos analizados.
    informe_resumen.write('\n El promedio se realizó para una cantidad de '+str(cantidad_audios)+' audios')
    informe_resumen.write('\n\\end{document}')
    informe_resumen.close()

    # Se genera el archivo resumen en formato .pdf.
    funciones.latex(funciones.DIRECTORIOS["pdf"], funciones.DIRECTORIOS["latex"] + nombre_salida)

def distancia_euclidea(archivo, ruta_ahocoder, cantidad_mfcc):
    '''Calcula las distancias euclideanas entre los diferentes coeficientes cepstrales
    y se escriben los resultados en el informe total.'''

    # Indica si la imagen es para audios sucios o fitrados.
    if ruta_ahocoder == funciones.DIRECTORIOS["mfccAhocoderRuido"]:
        identificador_imagen = "ruido"
    else:
        identificador_imagen = "filtrado"

    # Almacena el promedio de las distancias para cada MFCC de cada archivo.
    ponderado_distancia_euclidea = np.zeros(cantidad_mfcc)

    # Rutas con los archivos csv con los valores de los MFCC.
    # ruta_comparar pueden ser archivos de audio con ruido o filtrados.
    ruta_limpios = funciones.DIRECTORIOS["mfccAhocoder"]
    ruta_comparar = ruta_ahocoder

    # Lista de los archivos sucios o filtrados.
    lista_archivos_comparar = sorted(os.listdir(ruta_comparar))

    # Indica qué archivo sucio o filtrado se está analizando.
    posicion = 0

    #Selecciona los archivos a comparar alfabéticamente.
    for archivo_audio_limpio in sorted(os.listdir(ruta_limpios)):

        # Rutas absolutas de los archivos analizados.
        ruta_absoluta_limpios = os.path.join(ruta_limpios, archivo_audio_limpio)
        archivo_comparar = lista_archivos_comparar[posicion]
        ruta_absoluta_comparar = os.path.join(ruta_comparar, archivo_comparar)

        # Siguiente archivo a comparar.
        posicion = posicion + 1

        # Nombre de los archivos analizados para el informe.
        nombre_limpio = archivo_audio_limpio.replace("_", "\\_")
        nombre_comparar = archivo_comparar.replace("_", "\\_")

        # MFCC de los archivos de audio.
        mfcc_limpios = pd.read_csv(ruta_absoluta_limpios, sep='\t', header=None)
        mfcc_comparar = pd.read_csv(ruta_absoluta_comparar, sep='\t', header=None)

        # Se eliminan los valores del coeficiente de energía.
        mfcc_limpios = mfcc_limpios.drop(mfcc_limpios.columns[0], axis=1)
        mfcc_comparar = mfcc_comparar.drop(mfcc_comparar.columns[0], axis=1)

        # Se eliminan datos filas extras para poder calcular las distancias.
        filas_extra = mfcc_limpios.shape[0] - mfcc_comparar.shape[0]

        if filas_extra > 0:
            mfcc_limpios.drop(mfcc_limpios.tail(filas_extra).index, inplace=True)

        if filas_extra < 0:
            mfcc_comparar.drop(mfcc_comparar.tail(abs(filas_extra)).index, inplace=True)

        # Inicio de la sección de distancia euclídea.
        archivo.write('\\newpage')
        archivo.write('\\subsection{Métrica Distancia Euclídea para MFCC '+str(posicion)+'}' + '\n\n')
        archivo.write('\\textbf{Los archivos analizados son:} \n\n')
        archivo.write('\\begin{enumerate} \n' + '\\item ' + nombre_limpio + '\n')
        archivo.write('\\item ' + nombre_comparar + '\n' + '\\end{enumerate} \n\n')
        archivo.write('\\textbf{Las distancias para cada MFCC son: }' + '\n\n')

        # Lista para almacenar las distancias euclídeas.
        distancias = []

        for coeficiente in range(0, cantidad_mfcc):

            # Valores del MFCC.
            coeficiente_limpio = mfcc_limpios.iloc[:, coeficiente].values
            coeficiente_comparar = mfcc_comparar.iloc[:, coeficiente].values

            # Se calcula la distancia euclídea.
            distancia_euclideana = np.linalg.norm(coeficiente_limpio - coeficiente_comparar)

            # Se agrega la distancia calculada a la lista.
            distancias.append(distancia_euclideana)

        # Se convierte la lista de distancias en un arreglo.
        arreglo_distancia = np.asarray(distancias)

        # Se suman los resultados de todos los MFCC a los resultados de los demás archivos.
        ponderado_distancia_euclidea = arreglo_distancia + ponderado_distancia_euclidea

        #Inicio de tabla para latex
        archivo.write('\\begin{table}[H] \n')
        archivo.write('\\centering \n')
        archivo.write('\\caption{Distancia euclidiana para MFCC:'+str(posicion)+'}')
        archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
        archivo.write('\\hline \n')
        archivo.write('MFCC & Distancia euclídea & MFCC & Distancia euclídea \\\\')
        archivo.write('\\hline \n')

        # Se genera la tabla con los resultados.
        generar_tabla(archivo, cantidad_mfcc, distancias)

        # Cantidad de muestras por MFCC.
        cantidad_de_muestras = str(len(coeficiente_limpio))
        archivo.write('\n\\textbf{Con un número de muestras de:} ' + cantidad_de_muestras + '\n\n')

        # Se genera un gráfico para el MFCC.
        cepstros = np.arange(len(distancias))
        plt.bar(cepstros, distancias)
        plt.title('Distancia Euclidea')
        plt.xlabel('Coeficientes Cepstrales')
        plt.ylabel('Distancia Euclidea')
        plt.savefig(funciones.DIRECTORIOS["pictures"] + '/'+str(posicion)+ '_'+ identificador_imagen +'.png')
        archivo.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\textwidth]'+
                      '{' + funciones.DIRECTORIOS["pictures"] + '/'+str(posicion)+
                      '_'+ identificador_imagen +'.png' + '}\n')
        archivo.write('\\caption{Distancia euclídea: MFCC '+str(posicion)+'}\n \\label'+
                      '{fig:'+str(posicion)+'}\n \\end{figure}')
        plt.close()

    return ponderado_distancia_euclidea

def mean_absolute_distance(cola, archivo, ruta_ahocoder, cantidad_mfcc):
    '''Calcula las MAD entre los diferentes coeficientes cepstrales
    y se escriben los resultados en el informe total.'''

    archivo.write('\\newpage \n')

    # Rutas con los archivos csv con los valores de los MFCC.
    # ruta_comparar pueden ser archivos de audio con ruido o filtrados.
    ruta_limpios = funciones.DIRECTORIOS["mfccAhocoder"]
    ruta_comparar = ruta_ahocoder

    # Lista de los archivos sucios o filtrados.
    lista_archivos_comparar = sorted(os.listdir(ruta_comparar))

    # Indica qué archivo sucio o filtrado se está analizando.
    posicion = 0

    # Almacena el promedio de las distancias para cada MFCC de cada archivo.
    arreglo_ponderado_mad = np.zeros(cantidad_mfcc)

    # Almacena el promedio de la mad total por archivo.
    ponderado_mad = 0.0

    #Selecciona los archivos a comparar alfabéticamente.
    for archivo_audio_limpio in sorted(os.listdir(ruta_limpios)): #selecciona los archivos a comparar alfabeticamente

        # Rutas absolutas de los archivos analizados.
        ruta_absoluta_limpios = os.path.join(ruta_limpios, archivo_audio_limpio)
        archivo_comparar = lista_archivos_comparar[posicion]
        ruta_absoluta_comparar = os.path.join(ruta_comparar, archivo_comparar)

        # Siguiente archivo a comparar.
        posicion = posicion + 1

        # Nombre de los archivos analizados para el informe.
        nombre_limpio = archivo_audio_limpio.replace("_", "\\_")
        nombre_comparar = archivo_comparar.replace("_", "\\_")

        # MFCC de los archivos de audio.
        mfcc_limpios = pd.read_csv(ruta_absoluta_limpios, sep='\t', header=None) #leer el archivo
        mfcc_comparar = pd.read_csv(ruta_absoluta_comparar, sep='\t', header=None)

        # Se eliminan los valores del coeficiente de energía.
        mfcc_limpios = mfcc_limpios.drop(mfcc_limpios.columns[0], axis=1)
        mfcc_comparar = mfcc_comparar.drop(mfcc_comparar.columns[0], axis=1)

        # Se eliminan datos filas extras para poder calcular las distancias.
        filas_extra = mfcc_limpios.shape[0] - mfcc_comparar.shape[0]

        if filas_extra > 0:
            mfcc_limpios.drop(mfcc_limpios.tail(filas_extra).index, inplace=True)

        if filas_extra < 0:
            mfcc_comparar.drop(mfcc_comparar.tail(abs(filas_extra)).index, inplace=True)

        # Inicio de la sección de mad.
        archivo.write('\\subsection{MAD para MFCC '+str(posicion)+'}' + '\n\n')
        archivo.write('\\textbf{Los archivos analizados son:} \n\n')
        archivo.write('\\begin{enumerate} \n' + '\\item ' + nombre_limpio + '\n')
        archivo.write('\\item ' + nombre_comparar + '\n' + '\\end{enumerate} \n\n')

        # Lista para almacenar las mad.
        lista_mad = []

        for coeficiente in range(0, cantidad_mfcc):

            # Valores del MFCC.
            coeficiente_limpio = mfcc_limpios.iloc[:, coeficiente].values
            coeficiente_comparar = mfcc_comparar.iloc[:, coeficiente].values

            # Valores absolutos de las distancias entre los valores de los MFCC.
            diferencias_absolutas = abs(coeficiente_limpio - coeficiente_comparar)

            # Suma de las distancias absolutas.
            suma = 0
            for diferencia in diferencias_absolutas:
                suma = suma + diferencia

            # Se almacena el promedio de las distancias absolutas para un coeficiente.
            lista_mad.append(suma / len(diferencias_absolutas))

        # Suma de las mad de todos los archivos.
        suma_promedio = 0
        for promedio_distancia in lista_mad:
            suma_promedio = suma_promedio + promedio_distancia

        # MAD única resultado del promedio de todas las mad's de los archivos.
        mad_total = suma_promedio / cantidad_mfcc

        archivo.write('\\textbf{Los resultados para la métrica MAD son: } ' + '\n\n')
        archivo.write('\nMAD: ' + str(round(mad_total, 3)) + '\n\n')
        archivo.write('\\textbf{La distancia media absoluta es:} ' + '\n\n ')

        #Inicio de tabla para latex
        archivo.write('\\begin{table}[H] \n')
        archivo.write('\\centering \n')
        archivo.write('\\caption{Distancia media absoluta para MFCC:'+str(posicion)+'}\n')
        archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
        archivo.write('\\hline \n')
        archivo.write('MFCC & MAD & MFCC & MAD \\\\')
        archivo.write('\\hline \n')

        # Se genera la tabla con los resultados.
        generar_tabla(archivo, cantidad_mfcc, lista_mad)

        # Cantidad de muestras por MFCC.
        muestras = str(len(coeficiente_limpio))
        archivo.write('\n\\textbf{Número de muestras: } ' + muestras +'\n\n')
        archivo.write('\\newpage \n')

        # Se convierte la lista de mad's en un arreglo.
        arreglo_mad = np.asarray(lista_mad)

        # Promedio de las mad's por coeficiente.
        arreglo_ponderado_mad = arreglo_mad + arreglo_ponderado_mad

        # Promedio de las mad's totales por archivo.
        ponderado_mad = mad_total + ponderado_mad

    # Se colocan los valores a retornar en la cola.
    cola.put((arreglo_ponderado_mad, ponderado_mad))

def distancia_manhattan(queue, archivo, ruta_ahocoder, cantidad_mfcc):
    '''Calcula las distancias manhattan entre los diferentes coeficientes cepstrales
    y se escriben los resultados en el informe total.'''

    # Rutas con los archivos csv con los valores de los MFCC.
    # ruta_comparar pueden ser archivos de audio con ruido o filtrados.
    ruta_limpios = funciones.DIRECTORIOS["mfccAhocoder"]
    ruta_comparar = ruta_ahocoder

    # Lista de los archivos sucios o filtrados.
    lista_archivos_comparar = sorted(os.listdir(ruta_comparar))

    # Indica qué archivo sucio o filtrado se está analizando.
    posicion = 0

    # Almacena el promedio de las distancias manhattan para cada MFCC de cada archivo.
    ponderado_manhattan = np.zeros(cantidad_mfcc)

    # Selecciona los archivos a comparar alfabéticamente.
    for archivo_audio_limpio in sorted(os.listdir(ruta_limpios)): #selecciona los archivos a comparar alfabeticamente

        # Rutas absolutas de los archivos analizados.
        ruta_absoluta_limpios = os.path.join(ruta_limpios, archivo_audio_limpio)
        archivo_comparar = lista_archivos_comparar[posicion]
        ruta_absoluta_comparar = os.path.join(ruta_comparar, archivo_comparar)

        # Siguiente archivo a comparar.
        posicion = posicion + 1

        # Nombre de los archivos analizados para el informe.
        nombre_limpio = archivo_audio_limpio.replace("_", "\\_")
        nombre_comparar = archivo_comparar.replace("_", "\\_")

        # MFCC de los archivos de audio.
        mfcc_limpios = pd.read_csv(ruta_absoluta_limpios, sep='\t', header=None)
        mfcc_comparar = pd.read_csv(ruta_absoluta_comparar, sep='\t', header=None)

        # Se eliminan los valores del coeficiente de energía.
        mfcc_limpios = mfcc_limpios.drop(mfcc_limpios.columns[0], axis=1)
        mfcc_comparar = mfcc_comparar.drop(mfcc_comparar.columns[0], axis=1)

        # Se eliminan datos filas extras para poder calcular las distancias.
        filas_extra = mfcc_limpios.shape[0] - mfcc_comparar.shape[0]

        if filas_extra > 0:
            mfcc_limpios.drop(mfcc_limpios.tail(filas_extra).index, inplace=True)

        if filas_extra < 0:
            mfcc_comparar.drop(mfcc_comparar.tail(abs(filas_extra)).index, inplace=True)

        # Inicio de la sección de distancia manhattan.
        archivo.write('\\newpage \n') #tex
        archivo.write('\\subsection{Distancia Manhattan MFCC '+str(posicion)+'}' + '\n\n')
        archivo.write('\\textbf{Los archivos analizados son:} \n\n')
        archivo.write('\\begin{enumerate} \n' + '\\item ' + nombre_limpio + '\n')
        archivo.write('\\item ' + nombre_comparar + '\n' + '\\end{enumerate} \n\n')

        # Lista para almacenar las distancias manhattan.
        lista_manhattan = []

        for coeficiente in range(0, cantidad_mfcc):

            # Valores del MFCC.
            coeficiente_limpio = mfcc_limpios.iloc[:, coeficiente].values
            coeficiente_comparar = mfcc_comparar.iloc[:, coeficiente].values

            # Valores absolutos de las distancias entre los valores de los MFCC.
            distancias_manhattan = abs(coeficiente_limpio - coeficiente_comparar)

            # Suma de las distancias manhattan.
            suma = 0
            for distancia in distancias_manhattan: #Para obtener la suma de los componentes de un vector
                suma = suma + distancia

            # Se almacena la suma de la distancia manhattan de todos los coeficientes.
            lista_manhattan.append(suma)

        archivo.write('\\textbf{Los resultados para la métrica Distancia Manhattan son: } ' + '\n\n')

        #Inicio de tabla para latex
        archivo.write('\\begin{table}[H] \n')
        archivo.write('\\centering \n')
        archivo.write('\\caption{Distancia Manhattan para MFCC:'+str(posicion)+'}\n')
        archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
        archivo.write('\\hline \n')
        archivo.write('MFCC & Manhattan & MFCC & Manhattan \\\\')
        archivo.write('\\hline \n')

        # Se genera la tabla con los resultados.
        generar_tabla(archivo, cantidad_mfcc, lista_manhattan)

        # Cantidad de muestras por MFCC.
        muestras = str(len(coeficiente_limpio))
        archivo.write('\n\\textbf{Número de muestras: } ' + muestras +'\n\n')
        archivo.write('\\newpage \n')

        # Se convierte la lista de distancias manhattan en un arreglo.
        arreglo_manhattan = np.asarray(lista_manhattan)

        # Se suman los resultados de todos los MFCC a los resultados de los demás archivos.
        ponderado_manhattan = arreglo_manhattan + ponderado_manhattan

    # Se colocan los valores a retornar en la cola.
    queue.put(ponderado_manhattan)

def analisis_espectral(ruta_hexadecimal, ruta_ahocoder, cantidad_audios,
                       opciones, nombre_salida_informe, nombre_salida_resumen, cantidad_mfcc):
    ''' Función principal que llama a la aplicación de métricas espectrales,
    para lo cual provee la cantidad de audios a analizar, las rutas de los
    audios a comparar, las métricas a aplicar y el nombre de salida del informe.'''

    archivo = open(funciones.DIRECTORIOS["latex"] + nombre_salida_informe, 'w')
    archivo.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage{graphicx}'+
                  '\n\\usepackage{float}\n\\title{Sistema de análisis espectral entre conjuntos de '+
                  'señales de habla: Métrica Distancia Euclídea, Distancia absoluta media (MAD) y '+
                  'Distancia de Manhattan}\n\\author{EIE}\n\\begin{document}\n\\maketitle\n')

    # Se obtienen los valores de los MFCC en archivos csv.
    funciones.ahocoder_a_csv_espectral(funciones.DIRECTORIOS["mfcchexadecimal"],
                                       funciones.DIRECTORIOS["mfccAhocoder"], cantidad_mfcc)
    funciones.ahocoder_a_csv_espectral(ruta_hexadecimal, ruta_ahocoder, cantidad_mfcc)

    # Distancia euclídea.
    if opciones[0]:
        archivo.write('\\newpage \n')
        archivo.write('\\section{Distancia Euclídea} \n')
        ponderado_distancia_euclidea = distancia_euclidea(archivo, ruta_ahocoder, cantidad_mfcc)

    else:
        ponderado_distancia_euclidea = 0.0

    # MAD.
    if opciones[1]:
        archivo.write('\\newpage \n')
        archivo.write('\\section{MAD} \n')

        # Cola para almacenar los valores que retorna la métrica.
        retornados_mad = Queue.Queue()

        thread_mad = threading.Thread(target=mean_absolute_distance(retornados_mad,
                                                                    archivo, ruta_ahocoder,
                                                                    cantidad_mfcc))
        thread_mad.start()
        thread_mad.join()
        arreglo_ponderado_mad, ponderado_mad = retornados_mad.get()
    else:
        arreglo_ponderado_mad = 0.0
        ponderado_mad = 0.0

    # Distancia Manhattan.
    if opciones[2]:
        archivo.write('\\newpage \n')
        archivo.write('\\section{Distancia de Manhattan} \n')

        # Cola para almacenar los valores que retorna la métrica.
        retornados_manhattan = Queue.Queue()

        thread_manhattan = threading.Thread(target=distancia_manhattan(retornados_manhattan,
                                                                       archivo, ruta_ahocoder,
                                                                       cantidad_mfcc))
        thread_manhattan.start()
        thread_manhattan.join()
        ponderado_manhattan = retornados_manhattan.get()
    else:
        ponderado_manhattan = 0.0
    archivo.write('\n\\end{document}')
    archivo.close()

    # Se genera el informe resumen.
    generar_informe_resumen(cantidad_audios,
                            opciones, ponderado_distancia_euclidea,
                            ponderado_mad, arreglo_ponderado_mad,
                            ponderado_manhattan, nombre_salida_resumen,
                            cantidad_mfcc)

    # Se genera el informe completo.
    funciones.latex(funciones.DIRECTORIOS["pdf"], funciones.DIRECTORIOS["latex"] + nombre_salida_informe)
