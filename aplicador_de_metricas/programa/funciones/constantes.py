#!/usr/bin/env python
# -*- encoding: utf-8 -*-

'''Nombres para los informes.'''

# Nombres de los reportes generados.
INFORME_ESPECTRAL_RUIDO = "/informe_espectral_ruido.tex"
INFORME_RESUMEN_ESPECTRAL_RUIDO = "/informe_resumen_espectral_ruido.tex"
INFORME_ESPECTRAL_FILTRADO = "/informe_espectral_filtrado.tex"
INFORME_RESUMEN_ESPECTRAL_FILTRADO = "/informe_resumen_espectral_filtrado.tex"
INFORME_TONO_RUIDO = "/informe_tono_ruido.tex"
INFORME_RESUMEN_TONO_RUIDO = "/informe_resumen_tono_ruido.tex"
INFORME_TONO_FILTRADO = "/informe_tono_filtrado.tex"
INFORME_RESUMEN_TONO_FILTRADO = "/informe_resumen_tono_filtrado.tex"
