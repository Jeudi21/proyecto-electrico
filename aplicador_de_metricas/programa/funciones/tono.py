#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''Archivo que maneja toda la lógica para el análisis de tono.'''

import os
import statistics
import numpy as np
import Signal_Analysis.features.signal as safs
import funciones

def generar_informe_resumen(listas_resumen, nombre_salida_resumen, opciones):
    '''Genera un informe resumen con los datos promedios de las métricas aplicadas
    a todos los archivos.'''
    informe_resumen = open(funciones.DIRECTORIOS["latex"] + nombre_salida_resumen, 'w')
    informe_resumen.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\'+
                          'usepackage{graphicx}\n\\usepackage{float}\n\\title{Informe resumen: '+
                          'Sistema de '+ 'análisis de tono entre conjuntos de señales de habla.}\n\\author{EIE}\n\\'+
                          'begin{document}\n\\maketitle\n')
    informe_resumen.write('\\newpage')

    if opciones[0]:
        # Se escriben el resultado promedio de VDE.
        informe_resumen.write('\\section{Resultado de aplicar la métrica VDE}' + '\n\n')
        informe_resumen.write('Resultado = '+str(round(statistics.mean(listas_resumen["vde"]), 3)))

    if opciones[1]:
        # Se escriben el resultado promedio de diferencia.
        informe_resumen.write('\\section{Resultado de aplicar la métrica diferencia}' + '\n\n')
        informe_resumen.write('Resultado = '+str(round(statistics.mean(listas_resumen["diferencia"]), 3)))

    if opciones[2]:
        # Se escriben los resultados promedios de HNR.
        informe_resumen.write('\\section{Resultados de aplicar la métrica HNR}' + '\n\n')
        informe_resumen.write('Resultado natural = '+str(round(statistics.mean(listas_resumen["hnr_natural"]), 3))+'\n')
        informe_resumen.write('Resultado detectada = '+str(round(statistics.mean(listas_resumen["hnr_detectada"]), 3)))

    informe_resumen.write('\n\\end{document}')
    informe_resumen.close()

    # Se genera el archivo resumen en formato .pdf.
    funciones.latex(funciones.DIRECTORIOS["pdf"], funciones.DIRECTORIOS["latex"] + nombre_salida_resumen)

def vde(archivo, natural, detectada, cantidad_elementos, archivo_limpio,
        archivo_a_comparar, lista_resumen):
    '''Esta es la métrica de error de decisión de voz.
    Se recorren los vectores de voz natural y detectada, y se comparan.
    Se aumenta el contador de error al registar un fallo en la detección'''

    # Cantidad de errores de decisión.
    cantidad_error = 0

    # Si existen diferencias entre la voz detectada y la original,
    # se incrementa la cantidad de errores.
    for elemento in range(cantidad_elementos):
        if (natural[elemento] == 0 and detectada[elemento] != 0) or \
            (natural[elemento] != 0 and detectada[elemento] == 0):
            cantidad_error += 1

    # Se escriben los resultados en el informe.
    archivo.write('\\section{Resultados de aplicar la métrica VDE}' + '\n\n')
    archivo.write('\\textbf{Los archivos analizados son:}' + '\n\n')
    archivo.write('\\begin{enumerate} \n' + '\\item ' + archivo_limpio + '\n')
    archivo.write('\\item ' + archivo_a_comparar + '\n' + '\\end{enumerate} \n\n')
    archivo.write('Resultado = '+str(round(cantidad_error, 3)))
    lista_resumen.append(cantidad_error)

def diferencia(archivo, natural, detectada, cantidad_elementos,
               archivo_limpio, archivo_a_comparar, lista_resumen):
    """En esta metrica se recorren ambos vectores de voz natural
    y detectada y se suman todas las diferencias absolutas entre
    sus valores."""

    diferencia_total = 0

    for i in range(cantidad_elementos):
        diferencia_total += abs(natural[i] - detectada[i])

    archivo.write('\\section{Resultados de aplicar la métrica Diferencia}' + '\n\n')
    archivo.write('\\textbf{Los archivos analizados son:}' + '\n\n')
    archivo.write('\\begin{enumerate} \n' + '\\item ' + archivo_limpio + '\n')
    archivo.write('\\item ' + archivo_a_comparar + '\n' + '\\end{enumerate} \n\n')
    archivo.write('Resultado = '+str(round(diferencia_total, 3)))
    lista_resumen.append(diferencia_total)

def hnr(archivo, natural, detectada, archivo_limpio,
        archivo_a_comparar, lista_resumen_natural, lista_resumen_detectada):
    '''Obtiene la relación armónico-ruido entre la voz natural
    y la voz detectada.'''

    hnr_natural = safs.get_HNR(natural, 50000)
    hnr_detectada = safs.get_HNR(detectada, 50000)
    archivo.write('\\section{Resultados de aplicar la métrica HNR}' + '\n\n')
    archivo.write('\\textbf{Los archivos analizados son:}' + '\n\n')
    archivo.write('\\begin{enumerate} \n' + '\\item ' + archivo_limpio + '\n')
    archivo.write('\\item ' + archivo_a_comparar + '\n' + '\\end{enumerate} \n\n')
    archivo.write('Resultado natural = '+str(round(hnr_natural, 3))+'\n')
    archivo.write('Resultado detectada = '+str(round(hnr_detectada, 3)))
    lista_resumen_natural.append(hnr_natural)
    lista_resumen_detectada.append(hnr_detectada)

def aplicar_metrica(ruta_ahocoder, informe_latex, opcion, listas_resumen):
    '''Obtiene la información de los archivos de los directorios
    y aplica una o varias métricas de tono.'''

    posicion = 0
    informe_latex.write('\\newpage')

    # Rutas donde se encuentras los archivos a examinar.
    ruta_archivos_limpios = funciones.DIRECTORIOS["f0txt"]
    ruta_archivos_a_comparar = ruta_ahocoder

    # Lista de archivos sucios o filtrados.
    archivos_a_comparar = sorted(os.listdir(ruta_archivos_a_comparar))

    for archivo_audio_limpio in sorted(os.listdir(ruta_archivos_limpios)):

        # Archivo filtrado o sucio.
        archivo_a_comparar = archivos_a_comparar[posicion]

        # Rutas absolutas para los archivos.
        ruta_absoluta_limpio = os.path.join(ruta_archivos_limpios, archivo_audio_limpio)
        ruta_absoluta_comparar = os.path.join(ruta_archivos_a_comparar, archivo_a_comparar)

        # Vectores con los valores de la voz natural y la voz detectada.
        natural = np.genfromtxt(ruta_absoluta_limpio)
        detectada = np.genfromtxt(ruta_absoluta_comparar)

        # Se eliminan datos filas extras para poder calcular las distancias.
        filas_extra = natural.shape[0] - detectada.shape[0]

        if filas_extra > 0:
            natural = np.resize(natural, natural.size - filas_extra)

        if filas_extra < 0:
            detectada = np.resize(detectada, detectada.size + filas_extra)

        #Carga los vectores convertidos a frecuencia en natural_hertz y detectada_hertz.
        natural_hertz = pow(10, natural)
        detectada_hertz = pow(10, detectada)

        # Siguiente archivo sucio o filtrado.
        posicion = posicion + 1

        # Nombre de los archivos analizados para el informe.
        nombre_limpio = archivo_audio_limpio.replace("_", "\\_")
        nombre_comparar = archivo_a_comparar.replace("_", "\\_")

        # Se aplica la métrica según la opción ingresada.
        if opcion == "VDE":
            vde(informe_latex, natural_hertz,
                detectada_hertz, len(natural),
                nombre_limpio, nombre_comparar, listas_resumen["vde"])

        if opcion == "DIFERENCIA":
            diferencia(informe_latex, natural_hertz, detectada_hertz,
                       len(natural), nombre_limpio, nombre_comparar,
                       listas_resumen["diferencia"])

        if opcion == "HNR":
            hnr(informe_latex, natural_hertz, detectada_hertz,
                nombre_limpio, nombre_comparar,
                listas_resumen["hnr_natural"], listas_resumen["hnr_detectada"])

def tono(ruta_hexadecimal, ruta_ahocoder, opciones, nombre_salida, nombre_salida_resumen):
    ''' Función principal que llama a la aplicación de métricas de tono,
    para lo cual indica las métricas a aplicar, las rutas de los
    audios a comparar y el nombre de salida del informe.'''

    # Diccionario de listas que almacenan valores para el informe resumen.
    listas_resumen = {
        "vde": [],
        "diferencia": [],
        "hnr_natural": [],
        "hnr_detectada": []
    }

    # Se obtienen el vector de la voz natural.
    funciones.ahocoder_a_csv_tono(funciones.DIRECTORIOS["f0csv"], funciones.DIRECTORIOS["f0txt"])

    # Se obtiene el vector de la voz detectada.
    funciones.ahocoder_a_csv_tono(ruta_hexadecimal, ruta_ahocoder)

    informe_latex = open(funciones.DIRECTORIOS["latex"] + nombre_salida, 'w')
    informe_latex.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\'+
                        'usepackage{graphicx}\n\\usepackage{float}\n\\title{Sistema de '+
                        'análisis de tono entre conjuntos de señales de habla.}\n\\author{EIE}\n\\'+
                        'begin{document}\n\\maketitle\n')

    # Se analizan las opciones y se aplican las métricas.

    if opciones[0]:
        #Elección de VDE
        aplicar_metrica(ruta_ahocoder, informe_latex, "VDE", listas_resumen)

    if opciones[1]:
        #Elección de Diferencia
        aplicar_metrica(ruta_ahocoder, informe_latex, "DIFERENCIA", listas_resumen)

    if opciones[2]:
        #Elección de HNR
        aplicar_metrica(ruta_ahocoder, informe_latex, "HNR", listas_resumen)

    informe_latex.write('\n\\end{document}')
    informe_latex.close()

    # Se genera el informe de latex en formato pdf.
    funciones.latex(funciones.DIRECTORIOS["pdf"],
                    funciones.DIRECTORIOS["latex"] + nombre_salida)

    # Se genera el informe resumen.
    generar_informe_resumen(listas_resumen, nombre_salida_resumen, opciones)
