#!/usr/bin/env python
# -*- encoding: utf-8 -*-

'''Archivo para el manejo de directorios y procesamiento de archivos.'''

import os
import shutil
import datetime

# Nombre de los directorios con fecha de ejecución.
DIRECTORIOS = {}



def crear_dir():
    '''Crea todos los directorios necesarios para el programa.'''

    # Nombre base de los directorios.
    base_directorios = {
        "raiz": "../Analisis.",
        "resa_clean": "/resa_clean", "resa_ruido": "/resa_ruido",
        "resa_filtro": "/resa_filtro", "mfcchexadecimal": "/mfcchexadecimal",
        "mfcchexadecimalRuido": "/mfcchexadecimalRuido",
        "mfcchexadecimalFiltrado": "/mfcchexadecimalFiltrado",
        "f0csv": "/f0csv", "f0Rcsv": "/f0Rcsv", "f0filtrado": "/f0filtrado",
        "mfccAhocoder": "/mfccAhocoder", "mfccAhocoderRuido": "/mfccAhocoderRuido",
        "mfccAhocoderFiltrado": "/mfccAhocoderFiltrado", "f0txt": "/f0txt",
        "f0txtR": "/f0txtR", "f0txtFiltrado": "/f0txtFiltrado",
        "latex": "/latex", "pdf": "/pdf", "pictures": "/pictures"
    }

    # Fecha y hora del sistema.
    fecha_y_hora = datetime.datetime.now().strftime("%d-%m-%Y.%H:%M:%S")

    # Nombre del directorio raíz con fecha de ejecución.
    DIRECTORIOS["raiz"] = base_directorios["raiz"] + fecha_y_hora

    # Se crean los directorios del programa.
    for path in base_directorios:
        if path != "raiz":

            # Nombre del directorio con fecha de ejecución.
            DIRECTORIOS[path] = DIRECTORIOS["raiz"] + base_directorios[path]
            os.makedirs(DIRECTORIOS[path])

def del_dir():
    '''Elimina todos los directorios, excepto el de latex y pdf.
    En el caso del directorio pdf, solo se conservan los archivos .pdf.'''

    for path in DIRECTORIOS:
        if path != "pdf" and path != "latex" and path != "raiz" and path != "pictures":
            shutil.rmtree(DIRECTORIOS[path])
        if path == "pdf":
            lista_directorio = os.listdir(DIRECTORIOS[path])
            for filename in lista_directorio:
                if not filename.endswith(".pdf"):
                    os.remove(DIRECTORIOS[path] + "/" + filename)

def remuestreo(ruta, salida):
    '''Realiza el remuestreo de los audios de una ruta.'''

    # Archivos del directorio.
    lista_directorio = os.listdir(ruta)

    for filename in lista_directorio:
        outname = "temp_" + os.path.basename(filename)
        os.system("sox -G " + ruta + "/" + filename + " -r 16000 -b 16 " + salida + "/" + outname)

def ahocoder(ruta, salida_espec, salida_tono, cantidad_mfcc):
    '''Obtiene los MFCC y el tono de los archivos en una ruta.'''

    # Archivos del directorio.
    lista_directorio = os.listdir(ruta)

    for filename in lista_directorio:
        outname = os.path.splitext(filename)[0] + ".csv"
        os.system("./../ahocoder_64/ahocoder16_64 " +
                  ruta + "/" + filename + " " + salida_tono +
                  "/" + outname + " " + salida_espec + "/" + outname +
                  " --ccord=" + str(cantidad_mfcc))

def ahocoder_a_csv_espectral(ruta, salida, cantidad_mfcc):
    '''Obtiene los valores de los MFCC y los organiza en un
    archivo .csv.'''

    # Archivos del directorio.
    lista_directorio = os.listdir(ruta)

    for filename in lista_directorio:
        outname = os.path.splitext(filename)[0] + ".csv"
        os.system("./../SPTK/SPTK-3.11/SPTK-3.11/bin/x2x/x2x +fa +a"
                  + str(cantidad_mfcc + 1) + " " + ruta + "/" + filename
                  + " > " + salida + "/" + outname)

def ahocoder_a_csv_tono(ruta, salida):
    '''Obtiene los valores de los tonos y los organiza en un
    archivo .csv.'''

    # Archivos del directorio.
    lista_directorio = os.listdir(ruta)

    for filename in lista_directorio:
        outname = os.path.splitext(filename)[0] + ".csv"
        os.system("./../SPTK/SPTK-3.11/SPTK-3.11/bin/x2x/x2x +fa "
                  + ruta + "/" + filename + " > " + salida + "/"
                  + outname)

def latex(ruta_informe, ruta_tex):
    '''Crea un informe de LaTeX con formato .pdf.'''

    os.system("pdflatex -output-directory=" + ruta_informe + " " + ruta_tex)
