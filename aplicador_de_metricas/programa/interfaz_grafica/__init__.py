from .metricas import Ui_metricas, _translate
from .rutas import Ui_rutas
from .progreso import Ui_progreso
from .fin import Ui_fin
