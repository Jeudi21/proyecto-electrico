# Generar archivos .py con las ventanas de la interfaz

Los archivos .ui pueden ser abiertos en Qt Designer para realizar cambios a las ventanas de la interfaz.

## Generar archivos .py

Para generar los archivos .py con las ventanas se debe ejecutar el script de la siguiente manera

```
sh generar_ventanas.sh
```

Esto generara automáticamente los archivos en el directorio superior.

## Consideraciones

* Los archivos .py no deben ser modificados, ya que el script sobrescribe los archivos.

* Las ventanas pueden ser modificadas en el archivo programa.py, dado que este archivo se encarga de la lógica de la interfaz.

* Si los archivos .ui cambian de nombre, se deben cambiar los nombres de los import's en el archivo \__init\__.py de este directorio.
