#!/bin/bash

for FILE in ./*.ui;
do

OUTNAME=../`basename $FILE .ui`.py; 
pyuic4 "$FILE" -o "$OUTNAME"

done
