# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './fin.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_fin(object):
    def setupUi(self, fin):
        fin.setObjectName(_fromUtf8("fin"))
        fin.resize(635, 540)
        fin.setMinimumSize(QtCore.QSize(635, 540))
        fin.setMaximumSize(QtCore.QSize(635, 540))
        fin.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(19, 106, 138, 255), stop:1 rgba(38, 120, 113, 255));"))
        self.frame_2 = QtGui.QFrame(fin)
        self.frame_2.setGeometry(QtCore.QRect(80, 70, 477, 71))
        self.frame_2.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.frame_2.setFrameShape(QtGui.QFrame.Panel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label_6 = QtGui.QLabel(self.frame_2)
        self.label_6.setGeometry(QtCore.QRect(190, 20, 131, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(16)
        self.label_6.setFont(font)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.frame = QtGui.QFrame(fin)
        self.frame.setGeometry(QtCore.QRect(77, 67, 481, 391))
        self.frame.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.frame.setFrameShape(QtGui.QFrame.Panel)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setLineWidth(3)
        self.frame.setMidLineWidth(3)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.layoutWidget = QtGui.QWidget(self.frame)
        self.layoutWidget.setGeometry(QtCore.QRect(53, 93, 481, 211))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_10 = QtGui.QLabel(self.layoutWidget)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.gridLayout.addWidget(self.label_10, 1, 0, 1, 2)
        self.etiqueta_archivos = QtGui.QLabel(self.layoutWidget)
        self.etiqueta_archivos.setText(_fromUtf8(""))
        self.etiqueta_archivos.setObjectName(_fromUtf8("etiqueta_archivos"))
        self.gridLayout.addWidget(self.etiqueta_archivos, 1, 2, 1, 1)
        self.label_7 = QtGui.QLabel(self.layoutWidget)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout.addWidget(self.label_7, 0, 0, 1, 1)
        self.label_9 = QtGui.QLabel(self.layoutWidget)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.gridLayout.addWidget(self.label_9, 2, 0, 1, 1)
        self.etiqueta_tiempo = QtGui.QLabel(self.layoutWidget)
        self.etiqueta_tiempo.setText(_fromUtf8(""))
        self.etiqueta_tiempo.setObjectName(_fromUtf8("etiqueta_tiempo"))
        self.gridLayout.addWidget(self.etiqueta_tiempo, 2, 1, 1, 2)
        self.etiqueta_salida = QtGui.QLabel(self.layoutWidget)
        self.etiqueta_salida.setText(_fromUtf8(""))
        self.etiqueta_salida.setObjectName(_fromUtf8("etiqueta_salida"))
        self.gridLayout.addWidget(self.etiqueta_salida, 0, 1, 1, 2)
        self.layoutWidget1 = QtGui.QWidget(self.frame)
        self.layoutWidget1.setGeometry(QtCore.QRect(50, 320, 371, 30))
        self.layoutWidget1.setObjectName(_fromUtf8("layoutWidget1"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget1)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.btn_nuevo = QtGui.QPushButton(self.layoutWidget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(11)
        self.btn_nuevo.setFont(font)
        self.btn_nuevo.setStyleSheet(_fromUtf8("background-color:rgb(46, 255, 186);"))
        self.btn_nuevo.setObjectName(_fromUtf8("btn_nuevo"))
        self.horizontalLayout.addWidget(self.btn_nuevo)
        self.btn_salir = QtGui.QPushButton(self.layoutWidget1)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(11)
        self.btn_salir.setFont(font)
        self.btn_salir.setStyleSheet(_fromUtf8("background-color: rgb(255, 79, 79);"))
        self.btn_salir.setObjectName(_fromUtf8("btn_salir"))
        self.horizontalLayout.addWidget(self.btn_salir)
        self.frame.raise_()
        self.frame_2.raise_()

        self.retranslateUi(fin)
        QtCore.QMetaObject.connectSlotsByName(fin)

    def retranslateUi(self, fin):
        fin.setWindowTitle(_translate("fin", "Resultados", None))
        self.label_6.setText(_translate("fin", "Resultados", None))
        self.label_10.setText(_translate("fin", "Cantidad de archivos procesados por ruta:", None))
        self.label_7.setText(_translate("fin", "Directorio de salida:", None))
        self.label_9.setText(_translate("fin", "Tiempo de ejecución:", None))
        self.btn_nuevo.setText(_translate("fin", "Nuevo análisis", None))
        self.btn_salir.setText(_translate("fin", "Salir", None))

