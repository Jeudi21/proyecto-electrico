# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './metricas.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_metricas(object):
    def setupUi(self, metricas):
        metricas.setObjectName(_fromUtf8("metricas"))
        metricas.setWindowModality(QtCore.Qt.NonModal)
        metricas.resize(680, 568)
        metricas.setMinimumSize(QtCore.QSize(680, 568))
        metricas.setMaximumSize(QtCore.QSize(680, 568))
        metricas.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(19, 106, 138, 255), stop:1 rgba(38, 120, 113, 255));"))
        self.frame = QtGui.QFrame(metricas)
        self.frame.setGeometry(QtCore.QRect(90, 70, 491, 461))
        self.frame.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.frame.setFrameShape(QtGui.QFrame.Panel)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setLineWidth(3)
        self.frame.setMidLineWidth(3)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.btn_siguiente_eval = QtGui.QPushButton(self.frame)
        self.btn_siguiente_eval.setGeometry(QtCore.QRect(160, 390, 181, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(11)
        self.btn_siguiente_eval.setFont(font)
        self.btn_siguiente_eval.setStyleSheet(_fromUtf8("background-color:rgb(46, 255, 186);"))
        self.btn_siguiente_eval.setObjectName(_fromUtf8("btn_siguiente_eval"))
        self.btn_seleccionar_todas = QtGui.QPushButton(self.frame)
        self.btn_seleccionar_todas.setGeometry(QtCore.QRect(170, 92, 150, 30))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        self.btn_seleccionar_todas.setFont(font)
        self.btn_seleccionar_todas.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.btn_seleccionar_todas.setObjectName(_fromUtf8("btn_seleccionar_todas"))
        self.layoutWidget = QtGui.QWidget(self.frame)
        self.layoutWidget.setGeometry(QtCore.QRect(40, 130, 471, 191))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.casilla_euclidea = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_euclidea.setFont(font)
        self.casilla_euclidea.setObjectName(_fromUtf8("casilla_euclidea"))
        self.verticalLayout_2.addWidget(self.casilla_euclidea)
        self.casilla_mad = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_mad.setFont(font)
        self.casilla_mad.setObjectName(_fromUtf8("casilla_mad"))
        self.verticalLayout_2.addWidget(self.casilla_mad)
        self.casilla_manhattan = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_manhattan.setFont(font)
        self.casilla_manhattan.setObjectName(_fromUtf8("casilla_manhattan"))
        self.verticalLayout_2.addWidget(self.casilla_manhattan)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.casilla_hnr = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_hnr.setFont(font)
        self.casilla_hnr.setObjectName(_fromUtf8("casilla_hnr"))
        self.verticalLayout.addWidget(self.casilla_hnr)
        self.casilla_diferencia = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_diferencia.setFont(font)
        self.casilla_diferencia.setObjectName(_fromUtf8("casilla_diferencia"))
        self.verticalLayout.addWidget(self.casilla_diferencia)
        self.casilla_vde = QtGui.QCheckBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.casilla_vde.setFont(font)
        self.casilla_vde.setObjectName(_fromUtf8("casilla_vde"))
        self.verticalLayout.addWidget(self.casilla_vde)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.linea_mfcc = QtGui.QLineEdit(self.frame)
        self.linea_mfcc.setGeometry(QtCore.QRect(40, 330, 140, 20))
        self.linea_mfcc.setReadOnly(False)
        self.linea_mfcc.setObjectName(_fromUtf8("linea_mfcc"))
        self.frame_2 = QtGui.QFrame(metricas)
        self.frame_2.setGeometry(QtCore.QRect(93, 73, 487, 71))
        self.frame_2.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.frame_2.setFrameShape(QtGui.QFrame.Panel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(130, 20, 250, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(metricas)
        QtCore.QMetaObject.connectSlotsByName(metricas)

    def retranslateUi(self, metricas):
        metricas.setWindowTitle(_translate("metricas", "Seleccione las métricas de evaluación", None))
        self.btn_siguiente_eval.setText(_translate("metricas", "Siguiente", None))
        self.btn_seleccionar_todas.setText(_translate("metricas", "Seleccionar Todas", None))
        self.casilla_euclidea.setText(_translate("metricas", "Distancia Euclideana", None))
        self.casilla_mad.setText(_translate("metricas", "Distancia Absoluta Media", None))
        self.casilla_manhattan.setText(_translate("metricas", "Distancia Manhattan", None))
        self.casilla_hnr.setText(_translate("metricas", "Métrica HNR", None))
        self.casilla_diferencia.setText(_translate("metricas", "Criterio de Diferencia", None))
        self.casilla_vde.setText(_translate("metricas", "Métrica VDE", None))
        self.linea_mfcc.setPlaceholderText(_translate("metricas", "Cantidad de MFCC", None))
        self.label.setText(_translate("metricas", "Métricas de Evaluación", None))

