# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './progreso.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_progreso(object):
    def setupUi(self, progreso):
        progreso.setObjectName(_fromUtf8("progreso"))
        progreso.resize(962, 261)
        progreso.setMinimumSize(QtCore.QSize(962, 261))
        progreso.setMaximumSize(QtCore.QSize(962, 261))
        progreso.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(19, 106, 138, 255), stop:1 rgba(38, 120, 113, 255));"))
        self.frame = QtGui.QFrame(progreso)
        self.frame.setGeometry(QtCore.QRect(134, 72, 711, 141))
        self.frame.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.frame.setFrameShape(QtGui.QFrame.Panel)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setLineWidth(3)
        self.frame.setMidLineWidth(3)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.btn_siguiente = QtGui.QPushButton(self.frame)
        self.btn_siguiente.setGeometry(QtCore.QRect(270, 90, 181, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(11)
        self.btn_siguiente.setFont(font)
        self.btn_siguiente.setStyleSheet(_fromUtf8("background-color:rgb(46, 255, 186);"))
        self.btn_siguiente.setObjectName(_fromUtf8("btn_siguiente"))
        self.layoutWidget = QtGui.QWidget(self.frame)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 20, 671, 57))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.etiqueta_paso = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.etiqueta_paso.setFont(font)
        self.etiqueta_paso.setObjectName(_fromUtf8("etiqueta_paso"))
        self.verticalLayout.addWidget(self.etiqueta_paso)
        self.line = QtGui.QFrame(self.layoutWidget)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.barra_progreso = QtGui.QProgressBar(self.layoutWidget)
        self.barra_progreso.setMaximum(0)
        self.barra_progreso.setProperty("value", -1)
        self.barra_progreso.setTextVisible(False)
        self.barra_progreso.setInvertedAppearance(False)
        self.barra_progreso.setTextDirection(QtGui.QProgressBar.TopToBottom)
        self.barra_progreso.setObjectName(_fromUtf8("barra_progreso"))
        self.horizontalLayout.addWidget(self.barra_progreso)
        self.etiqueta_porcentaje = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.etiqueta_porcentaje.setFont(font)
        self.etiqueta_porcentaje.setObjectName(_fromUtf8("etiqueta_porcentaje"))
        self.horizontalLayout.addWidget(self.etiqueta_porcentaje)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(progreso)
        QtCore.QMetaObject.connectSlotsByName(progreso)

    def retranslateUi(self, progreso):
        progreso.setWindowTitle(_translate("progreso", "Analizando...", None))
        self.btn_siguiente.setText(_translate("progreso", "Siguiente", None))
        self.etiqueta_paso.setText(_translate("progreso", "Paso", None))
        self.etiqueta_porcentaje.setText(_translate("progreso", "100 %", None))

