# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './rutas.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_rutas(object):
    def setupUi(self, rutas):
        rutas.setObjectName(_fromUtf8("rutas"))
        rutas.resize(656, 522)
        rutas.setMaximumSize(QtCore.QSize(656, 522))
        rutas.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(19, 106, 138, 255), stop:1 rgba(38, 120, 113, 255));"))
        self.frame = QtGui.QFrame(rutas)
        self.frame.setGeometry(QtCore.QRect(87, 67, 481, 391))
        self.frame.setMinimumSize(QtCore.QSize(481, 391))
        self.frame.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.frame.setFrameShape(QtGui.QFrame.Panel)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setLineWidth(3)
        self.frame.setMidLineWidth(3)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.btn_analizar = QtGui.QPushButton(self.frame)
        self.btn_analizar.setGeometry(QtCore.QRect(150, 330, 181, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(11)
        self.btn_analizar.setFont(font)
        self.btn_analizar.setStyleSheet(_fromUtf8("background-color:rgb(46, 255, 186);"))
        self.btn_analizar.setObjectName(_fromUtf8("btn_analizar"))
        self.layoutWidget = QtGui.QWidget(self.frame)
        self.layoutWidget.setGeometry(QtCore.QRect(60, 80, 361, 241))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.linea_limpios = QtGui.QLineEdit(self.layoutWidget)
        self.linea_limpios.setObjectName(_fromUtf8("linea_limpios"))
        self.horizontalLayout.addWidget(self.linea_limpios)
        self.buscar_limpios = QtGui.QPushButton(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.buscar_limpios.setFont(font)
        self.buscar_limpios.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.buscar_limpios.setAutoExclusive(False)
        self.buscar_limpios.setObjectName(_fromUtf8("buscar_limpios"))
        self.horizontalLayout.addWidget(self.buscar_limpios)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.linea_sucios = QtGui.QLineEdit(self.layoutWidget)
        self.linea_sucios.setObjectName(_fromUtf8("linea_sucios"))
        self.horizontalLayout_2.addWidget(self.linea_sucios)
        self.buscar_sucios = QtGui.QPushButton(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.buscar_sucios.setFont(font)
        self.buscar_sucios.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.buscar_sucios.setObjectName(_fromUtf8("buscar_sucios"))
        self.horizontalLayout_2.addWidget(self.buscar_sucios)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.linea_filtrados = QtGui.QLineEdit(self.layoutWidget)
        self.linea_filtrados.setObjectName(_fromUtf8("linea_filtrados"))
        self.horizontalLayout_3.addWidget(self.linea_filtrados)
        self.buscar_filtrados = QtGui.QPushButton(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(10)
        self.buscar_filtrados.setFont(font)
        self.buscar_filtrados.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.buscar_filtrados.setObjectName(_fromUtf8("buscar_filtrados"))
        self.horizontalLayout_3.addWidget(self.buscar_filtrados)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.frame_2 = QtGui.QFrame(rutas)
        self.frame_2.setGeometry(QtCore.QRect(90, 70, 477, 71))
        self.frame_2.setStyleSheet(_fromUtf8("background-color: rgb(239, 239, 239);"))
        self.frame_2.setFrameShape(QtGui.QFrame.Panel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(160, 20, 181, 31))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Ebrima"))
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(rutas)
        QtCore.QMetaObject.connectSlotsByName(rutas)

    def retranslateUi(self, rutas):
        rutas.setWindowTitle(_translate("rutas", "Ingrese las rutas de los audios", None))
        self.btn_analizar.setText(_translate("rutas", "Analizar", None))
        self.linea_limpios.setPlaceholderText(_translate("rutas", "Ingrese la ruta de audios limpios", None))
        self.buscar_limpios.setText(_translate("rutas", "Buscar", None))
        self.linea_sucios.setPlaceholderText(_translate("rutas", "Ingrese la ruta de audios sucios", None))
        self.buscar_sucios.setText(_translate("rutas", "Buscar", None))
        self.linea_filtrados.setPlaceholderText(_translate("rutas", "Ingrese la ruta de audios filtrados", None))
        self.buscar_filtrados.setText(_translate("rutas", "Buscar", None))
        self.label.setText(_translate("rutas", "Rutas de audios", None))

