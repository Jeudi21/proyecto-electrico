#!/usr/bin/env python
# -*- encoding: utf-8 -*-

''''Archivo que instancia todas las ventanas de la interfaz gráfica, brinda
funciones a los botones y establece restricciones para el usuario.'''

import sys
import os
import glob
import time
import traceback
import shutil
import funciones

# sip se importa para que el texto de las rutas se pueda utilizar como strings.
import sip
sip.setapi('QString', 2)
from PyQt4 import QtGui, QtCore
# Archivos .py obtenidos de los archivos .ui de Qt Designer
# que contienen las ventanas.
import interfaz_grafica as ig

def verificacion_de_rutas(ruta):
    '''Altera banderas para indicar si se ingresó una ruta
    en una línea y si los archivos en esa ruta son de extensión
    .wav.'''

    # Indica si se ingresó una ruta.
    ruta_ingresada = False

    # Indica si la ruta contiene solo archivos .wav.
    es_archivo_wav = False
    cantidad_de_archivos = 0

    if ruta == "":
        return ruta_ingresada, es_archivo_wav, cantidad_de_archivos

    else:
        es_archivo_wav = True
        ruta_ingresada = True
        cantidad_de_archivos = len(glob.glob(ruta+"/*.wav"))

        lista_directorio = os.listdir(ruta)
        for nombre_archivo in lista_directorio:
            if not nombre_archivo.endswith(".wav"):
                es_archivo_wav = False

        return ruta_ingresada, es_archivo_wav, cantidad_de_archivos

class InterfazGrafica(QtGui.QMainWindow):
    ''''Clase que agrupa y configura todas las ventanas'''

    # Señal emitida por el thread cuando termina su función.
    indicador_fin_thread = QtCore.pyqtSignal()

    def __init__(self):
        '''Cuando se crea un objeto de la clase ventana, se genera
        la ventana de métricas.'''

        # Estado de las casillas de la ventana de métricas.
        self.estado_casillas = {}

        # Textos de las líneas para las rutas de la ventana de rutas.
        self.rutas_audios = {}

        self.ruta_limpios = None
        self.ruta_sucios = None
        self.progreso = None
        self.ui_final = None
        self.ui_rutas = None
        self.ui_progreso = None
        self.es_archivo_wav_sucio = None
        self.es_archivo_wav_limpio = None
        self.es_archivo_wav_filtrado = None
        self.cantidad_sucios = None
        self.cantidad_limpios = None
        self.cantidad_filtrados = None
        self.rutas = None
        self.ruta_filtrados = None
        self.filtrado = None
        self.flag_filtrado = None
        self.flag_sucio = None
        self.final = None
        self.thread = None
        self.tiempo_de_ejecucion = None
        self.cantidad_mfcc = 39
        self.error = False
        self.directorio_creado = False

        # Creación de la ventana métrica.
        super(InterfazGrafica, self).__init__()
        self.ventana_metricas()

        # Se modifica la acción evento de cierre de ventana para que ejecute
        #la función cierre_de_ventana.
        QtGui.QWidget.closeEvent = self.cierre_de_ventana

        # Cambio al estilo de la ventana.
        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create("Windows"))

    def ventana_metricas(self):
        ''''Crea una ventana de la clase Ui_metricas y configura
        sus botones.'''

        # Widget contenedor de la ventana.
        self.metricas = QtGui.QWidget()

        # Ventana de la clase Ui_metricas.
        self.ui_metricas = ig.Ui_metricas()

        # El widget contiene la ventana de métricas.
        self.ui_metricas.setupUi(self.metricas)

        # Configuración de botones de la ventana.
        # Los nombres de los botones están definidos en el archivo .py.
        self.ui_metricas.btn_seleccionar_todas.clicked.connect(self.seleccionar_casillas)
        self.ui_metricas.btn_siguiente_eval.clicked.connect(self.metricas_a_rutas)
        self.ui_metricas.linea_mfcc.setEnabled(False)

        # Se intercepta el evento de cambio en el estado de las casillas espectrales.
        self.ui_metricas.casilla_mad.stateChanged.connect(self.cambio_en_casillas)
        self.ui_metricas.casilla_euclidea.stateChanged.connect(self.cambio_en_casillas)
        self.ui_metricas.casilla_manhattan.stateChanged.connect(self.cambio_en_casillas)

        # Se muestra la ventana.
        self.metricas.show()

    def ventana_rutas(self):
        ''''Crea una ventana de la clase Ui_rutas y configura
        sus botones.'''

        # Widget contenedor de la ventana.
        self.rutas = QtGui.QWidget()

        # Ventana de la clase Ui_rutas.
        self.ui_rutas = ig.Ui_rutas()

        # El widget contiene la ventana de rutas.
        self.ui_rutas.setupUi(self.rutas)

        # Configuración de botones de la ventana.
        # Los nombres de los botones están definidos en el archivo .py.
        self.ui_rutas.buscar_limpios.clicked.connect(lambda: self.buscar_directorio("limpios"))
        self.ui_rutas.buscar_sucios.clicked.connect(lambda: self.buscar_directorio("sucios"))
        self.ui_rutas.buscar_filtrados.clicked.connect(lambda: self.buscar_directorio("filtrados"))
        self.ui_rutas.btn_analizar.clicked.connect(self.rutas_a_progreso)

        # Se muestra la ventana.
        self.rutas.show()

    def ventana_progreso(self):
        ''''Crea una ventana de la clase Ui_progreso y configura
        su botón.'''

        # Widget contenedor de la ventana.
        self.progreso = QtGui.QWidget()

        # Ventana de la clase Ui_progreso.
        self.ui_progreso = ig.Ui_progreso()

        # El widget contiene la ventana de progreso.
        self.ui_progreso.setupUi(self.progreso)

        # El botón de siguiente y el porcentaje se mostrarán
        # hasta que termine el proceso de análisis.
        self.ui_progreso.btn_siguiente.setVisible(False)
        self.ui_progreso.etiqueta_porcentaje.setVisible(False)
        self.ui_progreso.btn_siguiente.clicked.connect(self.progreso_a_fin)

        # Se muestra la ventana.
        self.progreso.show()

    def ventana_fin(self):
        ''''Crea una ventana de la clase Ui_fin y configuran
        sus botones.'''

        # Widget contenedor de la ventana.
        self.final = QtGui.QWidget()

        # Ventana de la clase Ui_fin.
        self.ui_final = ig.Ui_fin()

        # El widget contiene la ventana de fin.
        self.ui_final.setupUi(self.final)

        # Configuración de botones y etiquetas.
        self.ui_final.btn_salir.clicked.connect(sys.exit)
        self.ui_final.btn_nuevo.clicked.connect(self.fin_a_metricas)
        self.ui_final.etiqueta_salida.setText(ig._translate("", funciones.DIRECTORIOS["raiz"], None))
        self.ui_final.etiqueta_archivos.setText(str(self.cantidad_limpios))

        if self.tiempo_de_ejecucion < 60:
            self.ui_final.etiqueta_tiempo.setText(str(round(self.tiempo_de_ejecucion, 2)) + " segundos")
        else:
            self.ui_final.etiqueta_tiempo.setText(str(round(self.tiempo_de_ejecucion/60, 2)) + " minutos")

        # Se muestra la ventana.
        self.final.show()

    def metricas_a_rutas(self):
        ''''Realiza las acciones necesarias para la transición entre las
        ventanas de métricas y de rutas.'''

        # Se almacenan los estados de las casillas de las métricas.
        self.estado_casillas["Euclidea"] = self.ui_metricas.casilla_euclidea.checkState()
        self.estado_casillas["MAD"] = self.ui_metricas.casilla_mad.checkState()
        self.estado_casillas["Manhattan"] = self.ui_metricas.casilla_manhattan.checkState()
        self.estado_casillas["VDE"] = self.ui_metricas.casilla_vde.checkState()
        self.estado_casillas["Diferencia"] = self.ui_metricas.casilla_diferencia.checkState()
        self.estado_casillas["HNR"] = self.ui_metricas.casilla_hnr.checkState()

        if self.estado_casillas["Euclidea"] or self.estado_casillas["MAD"] or self.estado_casillas["Manhattan"]:

            # Si se eligió al menos una métrica espectral se verifica el estado
            # de entrada de cantidad de MFCC.
            contenido_linea_mfcc = self.ui_metricas.linea_mfcc.text()
            contiene_solo_numeros = contenido_linea_mfcc.isdecimal()

            if contenido_linea_mfcc != "" and contiene_solo_numeros and int(contenido_linea_mfcc) > 0:

                # Se almacena la cantidad de MFCC a generar.
                self.cantidad_mfcc = int(contenido_linea_mfcc)

                # Se modifica el atributo WA_DeleteOnClose de QtWidget para
                # que se elimine la ventana cuando se cierra.
                self.metricas.setAttribute(55, True)
                self.metricas.close()

                # Se crea y muestra la ventana rutas.
                self.ventana_rutas()

            else:
                # Si no se ingresó la cantidad de MFCC o no es un valor númerico mayor que 0,
                # se notifica al usuario mediante un mensaje.
                QtGui.QMessageBox.information(self, ig._translate("", "Entrada no válida", None),
                                              ig._translate("",
                                                            "Ingrese una entrada númerica mayor"
                                                            " que 0 en la cantidad de MFCC",
                                                            None),
                                              QtGui.QMessageBox.Ok)

        elif self.estado_casillas["VDE"] or self.estado_casillas["Diferencia"] or self.estado_casillas["HNR"]:

            # Se modifica el atributo WA_DeleteOnClose de QtWidget para
            # que se elimine la ventana cuando se cierra.
            self.metricas.setAttribute(55, True)
            self.metricas.close()

            # Se crea y muestra la ventana rutas.
            self.ventana_rutas()

        else:

            # Si no se eligió ninguna opción, no se realiza el cambio de ventanas.
            QtGui.QMessageBox.information(self, ig._translate("", "Ninguna métrica seleccionada", None),
                                          ig._translate("", "Elija al menos una opción en las casillas", None),
                                          QtGui.QMessageBox.Ok)

            return

    def rutas_a_progreso(self):
        ''''Realiza las acciones necesarias para la transición entre las
        ventanas de rutas y de progreso.'''

        # Se almacenan las rutas que se encuentran en las líneas.
        self.ruta_limpios = self.ui_rutas.linea_limpios.text()
        self.ruta_sucios = self.ui_rutas.linea_sucios.text()
        self.ruta_filtrados = self.ui_rutas.linea_filtrados.text()

        # El usuario no puede continuar si la ruta de audios limpios está
        # vacía o si la de audios limpios tiene un valor, pero alguna de
        # las otras dos están vacías.
        if len(self.ruta_limpios) is 0 or (len(self.ruta_sucios) is 0 and len(self.ruta_filtrados) is 0):
            QtGui.QMessageBox.information(self, ig._translate("", "Ruta vacía", None),
                                          ig._translate("",
                                                        "Para realizar el cálculo se debe ingresar direcciones en"
                                                        " audios limpios y en al menos una de las otras opciones.",
                                                        None),
                                          QtGui.QMessageBox.Ok)
            return

        # Se obtienen las banderas que indican el estado de las rutas
        # y de sus archivos.
        _, self.es_archivo_wav_limpio, self.cantidad_limpios =\
             verificacion_de_rutas(self.ruta_limpios)

        self.flag_sucio, self.es_archivo_wav_sucio, self.cantidad_sucios =\
             verificacion_de_rutas(self.ruta_sucios)

        self.flag_filtrado, self.es_archivo_wav_filtrado, self.cantidad_filtrados =\
             verificacion_de_rutas(self.ruta_filtrados)

        # Si la cantidad de archivos ingresados es la misma para ambas rutas
        # y solo contienen archivos de extensión .wav, entonces realiza el
        # cambio de ventanas.
        if (self.es_archivo_wav_limpio and self.flag_sucio and self.cantidad_limpios == self.cantidad_sucios\
            and self.es_archivo_wav_sucio) or (self.es_archivo_wav_limpio and self.flag_filtrado and\
            self.cantidad_limpios == self.cantidad_filtrados and self.es_archivo_wav_filtrado):

            self.rutas.setAttribute(55, True)
            self.rutas.close()
            self.ventana_progreso()

            # Se inicia el thread que realiza el análisis de los archivos
            # de audio y actualiza el estado en al ventana de progreso.
            self.inicio_thread_analizar()

        else:
            QtGui.QMessageBox.information(self, ig._translate("", "Problema con archivos", None),
                                          ig._translate("", "Las rutas deben tener la misma cantidad"
                                                        " de archivos y deben ser de extensión .wav.",
                                                        None),
                                          QtGui.QMessageBox.Ok)
            return

    def inicio_thread_analizar(self):
        ''' Se inicia el thread que permite realizar la extracción
        de características y aplicación de métricas mientras la
        ventana de progreso se mantiene animada.'''

        # Se crea un objetivo del tipo QThread.
        self.thread = QtCore.QThread(self)

        # Se indica cuál función ejecutará el thread.
        self.thread.run = self.analizar

        # Función que se ejecutará una vez terminado el thread.
        self.indicador_fin_thread.connect(self.ventana_progreso_final)

        # Se inicia el thread.
        self.thread.start()

    def analizar(self):
        ''' Realiza la extracción de características y aplicación
        de métricas.'''

        # Indica si ocurrió algún error en el análisis.
        self.error = False

        # Indica si el directorio para el programa se creó.
        self.directorio_creado = False

        try:
            # Se empieza a tomar el tiempo de ejecución.
            tiempo_de_inicio = time.time()

            # Indican si se aplicará alguna métrica de análisis espectral o de tono.
            flag_espectral = False
            flag_tono = False

            # De esta forma se modifica el texto que se muestra en
            # la ventana de progreso
            self.ui_progreso.etiqueta_paso.setText(ig._translate("", "Creando directorios", None))
            time.sleep(1)

            #Creación de directorio de salida.
            funciones.crear_dir()
            self.directorio_creado = True
            self.ui_progreso.etiqueta_paso.setText(ig._translate("", "Remuestreo de audios limpios", None))
            time.sleep(1)

            # Se resamplean los audios limpios.
            funciones.remuestreo(self.ruta_limpios, funciones.DIRECTORIOS["resa_clean"])
            self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                 "Extrayendo los MFCC en formato hexadecimal"
                                                                 " y la frecuencia fundamental de los audios limpios",
                                                                 None))
            time.sleep(1)

            # Se obtienen los MFCC y el tono fundamental de los audios limpios.
            funciones.ahocoder(funciones.DIRECTORIOS["resa_clean"],
                               funciones.DIRECTORIOS["mfcchexadecimal"],
                               funciones.DIRECTORIOS["f0csv"], self.cantidad_mfcc)

            if self.flag_sucio:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("", "Resampleando audios sucios", None))
                time.sleep(1)

                # Se resamplean los archivos sucios.
                funciones.remuestreo(self.ruta_sucios, funciones.DIRECTORIOS["resa_ruido"])
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Extrayendo los MFCC en formato hexadecimal"
                                                                     " y la frecuencia fundamental"
                                                                     " de los audios sucios",
                                                                     None))
                time.sleep(1)

                # Se obtienen los MFCC y el tono fundamental de los audios sucios.
                funciones.ahocoder(funciones.DIRECTORIOS["resa_ruido"],
                                   funciones.DIRECTORIOS["mfcchexadecimalRuido"],
                                   funciones.DIRECTORIOS["f0Rcsv"], self.cantidad_mfcc)

            if self.flag_filtrado:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Resampleando audios filtrados",
                                                                     None))
                time.sleep(1)

                # Se resamplean los audios filtrados.
                funciones.remuestreo(self.ruta_filtrados, funciones.DIRECTORIOS["resa_filtro"])
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Extrayendo MFCC en formato hexadecimal"
                                                                     " y la frecuencia fundamental de los"
                                                                     " audios filtrados",
                                                                     None))
                time.sleep(1)

                # Se obtienen los MFCC y el tono fundamental de los audios filtrados.
                funciones.ahocoder(funciones.DIRECTORIOS["resa_filtro"],
                                   funciones.DIRECTORIOS["mfcchexadecimalFiltrado"],
                                   funciones.DIRECTORIOS["f0filtrado"], self.cantidad_mfcc)

            # Se almacena el estado de las casillas de análisis espectral.
            if self.estado_casillas["Euclidea"] or self.estado_casillas["MAD"] or self.estado_casillas["Manhattan"]:
                flag_espectral = True
                opciones_espectral = [self.estado_casillas["Euclidea"],
                                      self.estado_casillas["MAD"],
                                      self.estado_casillas["Manhattan"]]

            # Se almacena el estado de las casillas de análisis de tono.
            if self.estado_casillas["VDE"] or self.estado_casillas["Diferencia"] or self.estado_casillas["HNR"]:
                flag_tono = True
                opciones_tono = [self.estado_casillas["VDE"],
                                 self.estado_casillas["Diferencia"],
                                 self.estado_casillas["HNR"]]

            # Se aplican las métricas espectrales a los audios sucios.
            if flag_espectral and self.flag_sucio:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Aplicando métricas espectrales a audios sucios",
                                                                     None))
                time.sleep(1)
                funciones.analisis_espectral(funciones.DIRECTORIOS["mfcchexadecimalRuido"],
                                             funciones.DIRECTORIOS["mfccAhocoderRuido"],
                                             self.cantidad_limpios, opciones_espectral,
                                             funciones.INFORME_ESPECTRAL_RUIDO,
                                             funciones.INFORME_RESUMEN_ESPECTRAL_RUIDO,
                                             self.cantidad_mfcc)

            # Se aplican las métricas espectrales a los audios filtrados.
            if flag_espectral and self.flag_filtrado:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Aplicando métricas espectrales"
                                                                     " a audios filtrados",
                                                                     None))
                time.sleep(1)
                funciones.analisis_espectral(funciones.DIRECTORIOS["mfcchexadecimalFiltrado"],
                                             funciones.DIRECTORIOS["mfccAhocoderFiltrado"],
                                             self.cantidad_limpios, opciones_espectral,
                                             funciones.INFORME_ESPECTRAL_FILTRADO,
                                             funciones.INFORME_RESUMEN_ESPECTRAL_FILTRADO,
                                             self.cantidad_mfcc)

            # Se aplican las métricas de tono a los audios sucios.
            if flag_tono and self.flag_sucio:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Aplicando métricas de tono a audios sucios",
                                                                     None))
                time.sleep(1)
                funciones.tono(funciones.DIRECTORIOS["f0Rcsv"], funciones.DIRECTORIOS["f0txtR"],
                               opciones_tono, funciones.INFORME_TONO_RUIDO,
                               funciones.INFORME_RESUMEN_TONO_RUIDO)

            # Se aplican las métricas de tono a los audios filtrados.
            if flag_tono and self.flag_filtrado:
                self.ui_progreso.etiqueta_paso.setText(ig._translate("",
                                                                     "Aplicando métricas de tono a audios filtrados",
                                                                     None))
                time.sleep(1)
                funciones.tono(funciones.DIRECTORIOS["f0filtrado"],
                               funciones.DIRECTORIOS["f0txtFiltrado"],
                               opciones_tono, funciones.INFORME_TONO_FILTRADO,
                               funciones.INFORME_RESUMEN_TONO_FILTRADO)

            self.ui_progreso.etiqueta_paso.setText(ig._translate("", "Eliminando directorios temporales", None))
            time.sleep(1)

            # Se eliminan los directorios y archivos temporales.
            funciones.del_dir()

            # Se termina la medición del tiempo de ejecución.
            self.tiempo_de_ejecucion = time.time() - tiempo_de_inicio

            # Se emite la señal de que el thread ha terminado.
            self.indicador_fin_thread.emit()

        except:

            # Se muestra la información del error.
            exc_info = sys.exc_info()
            traceback.print_exception(*exc_info)
            del exc_info

            # Se indica que ocurrió un error en la ejecución
            self.error = True

            # Se emite la señal de que el thread ha terminado.
            self.indicador_fin_thread.emit()

    def ventana_progreso_final(self):
        ''' Actualiza la ventana de progreso final cuando
        se termina el análisis de los audios'''

        # Se desconecta la función asociada a la señal para evitar
        # múltiples funciones conectadas a la señal debido a múltiples
        # ejecuciones.
        self.indicador_fin_thread.disconnect(self.ventana_progreso_final)

        # Si ocurrió un error, se elimina el directorio (si se creó), se muestra
        # un mensaje de error y se vuelve a la ventana de métricas.
        if self.error:

            if self.directorio_creado:
                shutil.rmtree(funciones.DIRECTORIOS["raiz"])

            QtGui.QMessageBox.critical(self, ig._translate("", "Error", None),
                                       ig._translate("", "Ocurrió un error en la ejecución del programa", None),
                                       QtGui.QMessageBox.Ok)

            self.progreso.setAttribute(55, True)
            self.progreso.close()
            self.ventana_metricas()

        else:

            # Se hacen visibles el porcentaje y el botón de siguiente.
            self.ui_progreso.btn_siguiente.setVisible(True)
            self.ui_progreso.etiqueta_porcentaje.setVisible(True)

            # La barra de progreso llega al 100 %.
            self.ui_progreso.barra_progreso.setMaximum(1)
            self.ui_progreso.barra_progreso.setValue(1)
            self.progreso.setWindowTitle(ig._translate("", "Análisis completado", None))
            self.ui_progreso.etiqueta_paso.setText("Completado")

    def progreso_a_fin(self):
        ''' Se pasa de la ventana de progreso a la ventana de fin.'''

        self.progreso.setAttribute(55, True)
        self.progreso.close()
        self.ventana_fin()

    def fin_a_metricas(self):
        ''' Se pasa de la ventana de fin a la ventana de métricas
        para iniciar un nuevo análisis.'''

        self.final.setAttribute(55, True)
        self.final.close()
        self.ventana_metricas()

    def seleccionar_casillas(self):
        ''' Selecciona todas las casillas de la ventana de métrica.'''

        self.ui_metricas.casilla_euclidea.setChecked(True)
        self.ui_metricas.casilla_mad.setChecked(True)
        self.ui_metricas.casilla_manhattan.setChecked(True)
        self.ui_metricas.casilla_hnr.setChecked(True)
        self.ui_metricas.casilla_vde.setChecked(True)
        self.ui_metricas.casilla_diferencia.setChecked(True)

    def buscar_directorio(self, tipo):
        ''' Permite la búsqueda de rutas de forma gráfica
        y coloca la ruta en la línea correspondiente.'''

        ruta = str(QtGui.QFileDialog.getExistingDirectory(self, "Seleccione el directorio"))
        if tipo == "limpios":
            self.ui_rutas.linea_limpios.setText(ruta)
        if tipo == "sucios":
            self.ui_rutas.linea_sucios.setText(ruta)
        if tipo == "filtrados":
            self.ui_rutas.linea_filtrados.setText(ruta)

    def cierre_de_ventana(self, event):
        ''' Si la ventana se cerró mediante el botón X,
        se intercepta la acción y se pregunta al usuario
        si realmente quiere detener el programa.'''

        if event.spontaneous():
            event.ignore()
            choice = QtGui.QMessageBox.question(self, "Salir",
                                                ig._translate("",
                                                              "¿Está seguro de abandonar el programa?",
                                                              None), QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            if choice == QtGui.QMessageBox.Yes:
                sys.exit()
            else:
                pass

    def cambio_en_casillas(self):
        ''' Cambia el estado de la línea para el ingreso de
        la cantidad de MFCC dependiendo del estado de las
        casillas de métricas espectrales.'''

        if self.ui_metricas.casilla_mad.checkState() or\
        self.ui_metricas.casilla_manhattan.checkState() or\
        self.ui_metricas.casilla_euclidea.checkState():

            self.ui_metricas.linea_mfcc.setEnabled(True)

        else:
            self.ui_metricas.linea_mfcc.setEnabled(False)

if __name__ == "__main__":
    APP = QtGui.QApplication(sys.argv)
    INTERFAZ = InterfazGrafica()
    sys.exit(APP.exec_())
